<?php
header("Content-tyle: application/json");
/**
 * @author Luis Carlos
 * @copyright 2010
 */

function lt($texto){
	$texto = addslashes($texto);
	$texto = trim($texto);
	$texto = htmlspecialchars($texto);
	$texto=mb_convert_encoding($texto, 'HTML-ENTITIES', 'UTF-8');
	$texto=str_replace('&amp;','&',$texto);
	
	return $texto;
}
function es_email($email){
    $mail_correcto = 0;
    //compruebo unas cosas primeras
    if ((strlen($email) >= 6) && (substr_count($email,"@") == 1) && (substr($email,0,1) != "@") && (substr($email,strlen($email)-1,1) != "@")){
       if ((!strstr($email,"'")) && (!strstr($email,"\"")) && (!strstr($email,"\\")) && (!strstr($email,"\$")) && (!strstr($email," "))) {
          //miro si tiene caracter .
          if (substr_count($email,".")>= 1){
             //obtengo la terminacion del dominio
             $term_dom = substr(strrchr ($email, '.'),1);
             //compruebo que la terminación del dominio sea correcta
             if (strlen($term_dom)>1 && strlen($term_dom)<5 && (!strstr($term_dom,"@")) ){
                //compruebo que lo de antes del dominio sea correcto
                $antes_dom = substr($email,0,strlen($email) - strlen($term_dom) - 1);
                $caracter_ult = substr($antes_dom,strlen($antes_dom)-1,1);
                if ($caracter_ult != "@" && $caracter_ult != "."){
                   $mail_correcto = 1;
                }
             }
          }
       }
    }
    if ($mail_correcto)
       return true;
    else
       return false;
} 

if(count($_POST) == 0){ exit(); }
$resultado = false;

if(!empty($_POST['nombre']) && !empty($_POST['email']) && !empty($_POST['comentario'])){
	if(es_email($_POST['email'])){
		$htmlenviar='<html>
			<head>
   			<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
               <title>Contacto de casa creativa</title>
			</head>
				<body>
				<p>Te han enviado un mensaje de contacto de kilometrox.com.mx.</p>
				<p>Una persona de nombre '.lt($_POST['nombre']).', te ha dejado un mensaje de contacto el dia '.date('d/m/Y H:i O').' y este es su mensaje:</p>
				<p>-----------------------------------------------------------------</p>
				<p>'.lt($_POST['comentario']).'</p>
				<p>-----------------------------------------------------------------</p>
				<p>Puede mandarle respuesta al correo '.lt($_POST['email']).'</p>
				<p>Su IP es: '.$_SERVER['REMOTE_ADDR'].'</p>
				<p><br /><br /><br /><br /><br /><br /><br />Con la tecnología de: <a title="Faxterol - Desarrollo Web" href="http://faxterol.com">Faxterol - Desarrollo Web</a></p>
				</body>
   			</html>';
			$asunto = 'Contacto KMX';
			$cabeceras = "From: noreply <noreply@kilometrox.com.mx>\r\nContent-type: text/html\r\nX-Priority: 1\r\n";
            //mail('makesurfer@gmail.com',$asunto,$htmlenviar,$cabeceras);
            mail('info.kilometrox@gmail.com',$asunto,$htmlenviar,$cabeceras);
			
			$resultado = true;
            $msjerror = 'Tu mensaje se ha enviado satisfactoriamente.';
		}
		else{
			$msjerror = 'El correo que insertaste no es valido.';
		}
		
}
else{
    
	$msjerror = 'Ups!! has dejado algún dato vacio, verifica bien por favor.';
}
$jsona=array('resultado' => $resultado,'msj' => $msjerror);
echo json_encode($jsona);
?>