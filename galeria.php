<?php
//extraemos con cURL el contenido del api de facebook 
$gals = curl_init();
curl_setopt_array($gals,array(
	CURLOPT_URL => "http://graph.facebook.com/140387082653939/photos?limit=100",
	CURLOPT_USERAGENT => "Faxterol 1.0 Windows es_MX / Chromium",
	CURLOPT_RETURNTRANSFER => 1
));

$galeria = curl_exec($gals);
curl_close($gals);

$galeria = json_decode($galeria);
$fotos = $galeria->data;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Galeria :: Kilometro X</title>
		<link href="estilo/estilo.css" rel="stylesheet" type="text/css" />
		<link href="estilo/jScrollPane.css" rel="stylesheet" type="text/css" />
		<link href="estilo/demoStyles.css" rel="stylesheet" type="text/css" />
		<meta name="subject" content="deportes extremos" />
	
		<meta name="Description" content="Mira las fotos del Kilometro X y vé como algunos de nuestros amigos se divirtieron al extremo." />
		<meta name="Keywords" content="deportes,extremos,alpinismo,rapel,gotcha,kilometro x, tepic, nayarit, cerro san juan, ecoturismo" />
		
		<meta name="Language" content="es-MX" />
		<meta name="Revisit-After" content="10 days" />
		<meta name="distribution" content="global" />
		<meta name="Robots" content="index,follow" />
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.mousewheel.js"></script>
		<script type="text/javascript" src="js/jScrollPane.js"></script>
		
		<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
		<script type="text/javascript" src="js/sexylightbox.v2.3.jquery.min.js"></script>
  		<link rel="stylesheet" href="estilo/sexylightbox.css" type="text/css" />
		
		<script type="text/javascript">
			$(document).ready(function(){
				SexyLightbox.initialize({color:'blanco', dir: 'sexyimages'});
				$('.scroll-pane').jScrollPane();
				
			});
		</script>
		
		<!--[if IE 6]>
    	<link href="estilo/estilo_ie6.css" rel="stylesheet" type="text/css" />
    	<![endif]-->
    	<!--[if IE 8]>
    	<style>
    	body{
    		position: absolute;
    	}
		</style>
    	<![endif]-->
    </head>
    <body>
    	
        <div id="cuerpo">
        	<h1><a title="Entrar a la pagina principal del Kilometro X" href="principal.html">Kilometro X</a></h1>
			<ul class="sociales">
				<li><a title="Kilometro X está en Facebook" target="_blank" href="http://www.facebook.com/pages/Aguacate-Nayarit/Kilometro-X/140384242654223?v=info&ref=search">Facebook</a></li>
				<li><a title="Mira nuestra galería de Fotos" href="galeria.php" class="gal">Galería de fotos.</a></li>
			</ul>
        	<p id="ac"><span>Musica On</span> <a title="Términos de uso." href="terminos_uso.html">Términos de uso</a></p>
        	<ul id="menu">
        		<li><a title="Actividades" class="t1" href="actividades.html">Actividades</a></li>
				<li><a title="Servicios" class="t2" href="servicios.html">Servicios</a></li>
				<li class="u"><a title="Ubicación" class="t3" href="ubicacion.html">Ubicación</a></li>
				<li><a title="Contacto" class="t4" href="contacto.html">Contacto</a></li>
				<li><a title="Descargas" class="t5" href="descargas.html">Descargas</a></li>
        	</ul>
			<div id="cont" class="galeria">
				<h2>Galeria</h2>
				<ul class="scroll-pane"><?php 
					foreach($fotos as $idfoto => $foto){
					?>
					<li><a rel="sexylightbox[kmx]" title="Foto <?=($idfoto+1);?> de Kilometro X" href="<?=$foto->source;?>"><img title="Foto <?=($idfoto+1);?> de Kilometro X" src="<?=$foto->picture;?>" /></a></li>
					<?php 
					}
					?>
				</ul>
			</div>
            <a title="Paquetes" href="paquetes.html" id="kmxpaq">Paquetes</a>
			<a title="http://www.kilometrox.com.mx" id="kmxfooter" href="http://www.kilometrox.com.mx">http://www.kilometrox.com.mx</a>
        </div>
    </body>
</html>
